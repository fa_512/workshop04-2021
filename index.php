<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Register</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php
  include('functions.php');

  $carreras = getCarrers();
  //print_r($carreras);
?>
<body>
  <div class="container-fluid">
    <div class="jumbotron">
      <h1 class="display-4">Register</h1>
      <p class="lead">This is the Register process</p>
      <hr class="my-4">
    </div>
    <form method="post" action="save.php">
    <div class="form-group">
        <label for="id_number">ID Number</label>
        <input id="id_number" class="form-control" type="text" name="id_number" placeholder="x xxxx xxxx">
      </div>
      <div class="form-group">
        <label for="name">First Name</label>
        <input id="name" class="form-control" type="text" name="name">
      </div>
      <div class="form-group">
        <label for="last-name">Last Name</label>
        <input id="last-name" class="form-control" type="text" name="lastname">
      </div>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input id="email" class="form-control" type="text" name="email">
      </div>
      <div class="form-group">
        <label for="carreras">Career</label>
        <select id="carreras" class="form-control" name="id_carrera">
          <?php
            foreach($carreras as $carrera) {
              echo "<option value = ".$carrera[0].">" .$carrera[2]."</option>";//imprime carreras
            }
            ?>
        </select>
        
      </div>
      <!-- <div class="form-group">
        <label for="password">Password</label>
        <input id="password" class="form-control" type="password" name="password">
      </div>-->
    
      <button type="submit" class="btn btn-primary" onclick="return"sended information"> Sign up </button>
      <a type="button" class="btn btn-info" href="listaMatricula.php">Lista de matricula</a><!--envia a listMatricula.php
       <input type="submit" class="btn btn-primary" value="Sign up"></input> -->
    </form>
  </div>

</body>
</html>